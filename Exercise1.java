import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class Exercise1 {
    public static void main(String[] args){
        HashMap<Integer, String> results = new HashMap<Integer, String>();
        Scanner input = new Scanner(System.in);
        Random r = new Random();
        Semaphore semaphore = new Semaphore(1);

        System.out.print("Insert array size: ");
        int N = input.nextInt();
        System.out.println();
        int[] arr = new int[N];

        for(int i = 0; i < N; i++){
            arr[i] = r.nextInt(201) - 100;
        }

        Thread t1 = new Thread(new T1(arr, results, semaphore));
        Thread t2 = new Thread(new T2(arr, results, semaphore));
        Thread t3 = new Thread(new T3(results));
        Thread t4 = new Thread(new T4(results));

        t1.start();
        t2.start();
        t3.start();

        try{t1.join();}
        catch(Exception e){e.printStackTrace();}
        try{t2.join();}
        catch(Exception e){e.printStackTrace();}
        try{t3.join();}
        catch(Exception e){e.printStackTrace();}

        t4.start();
    }
}
