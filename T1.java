import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class T1 extends Thread {
    int[] arr;
    ArrayList<Integer> changed;
    HashMap<Integer, String> results;
    int sum = 0, randIndex;
    Semaphore semaphore;

    public T1(int[] arr, HashMap<Integer, String> results, Semaphore semaphore)
    {
        this.arr = arr;
        this.results = results;
        this.semaphore = semaphore;
        this.changed = new ArrayList<Integer>();
    }

    public void run(){
        System.out.println("T1 started!");

        try{
            System.out.println("T1 acquiring lock...");
            semaphore.acquire();
            System.out.println("T1 got the lock!");

            try{
                for(int i = 0; i < arr.length/2; i++){
                    do{
                        randIndex = new Random().nextInt(arr.length);
                    }while(changed.contains(randIndex));
                    arr[randIndex] = new Random().nextInt(201) - 100;
                    changed.add(randIndex);
                }
            }finally {
                System.out.println("T1 releasing lock...");
                semaphore.release();
            }

        }
        catch (InterruptedException e){
            e.printStackTrace();
        }

        for(int i : arr) sum += i;

        results.put(sum, "T1");
        System.out.println("T1: " + sum);

        System.out.println("T1 finished!");
    }

}
