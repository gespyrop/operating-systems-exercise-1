import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class T2 extends Thread {
    int[] arr;
    ArrayList<Integer> changed;
    HashMap<Integer, String> results;
    int product = 1, randIndex;
    Semaphore semaphore;

    public T2(int[] arr, HashMap<Integer, String> results, Semaphore semaphore)
    {
        this.arr = arr;
        this.results = results;
        this.semaphore = semaphore;
        this.changed = new ArrayList<Integer>();
    }

    public void run(){
        System.out.println("T2 started!");

        try{
            System.out.println("T2 acquiring lock...");
            semaphore.acquire();
            System.out.println("T2 got the lock!");

            try{
                for(int i = 0; i < arr.length/2; i++){
                    do{
                        randIndex = new Random().nextInt(arr.length);
                    }while(changed.contains(randIndex));
                    arr[randIndex] = new Random().nextInt(201) - 100;
                    changed.add(randIndex);
                }
            }finally {
                System.out.println("T2 releasing lock...");
                semaphore.release();
            }

        }
        catch (InterruptedException e){
            e.printStackTrace();
        }

        for(int i : arr) product *= i;

        results.put(product, "T2");
        System.out.println("T2: " + product);

        System.out.println("T2 finished!");
    }

}
