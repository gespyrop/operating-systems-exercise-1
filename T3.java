import java.util.HashMap;
import java.util.Random;

public class T3 extends Thread {
    int rand;
    HashMap<Integer, String> results;

    public T3(HashMap<Integer, String> results){this.results = results;}

    public void run(){
        System.out.println("T3 started!");

        rand = new Random().nextInt(2001) - 1000;

        results.put(rand, "T3");
        System.out.println("T3: " + rand);

        System.out.println("T3 finished!");
    }

}
