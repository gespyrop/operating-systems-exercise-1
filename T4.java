import java.util.HashMap;

import static java.util.Arrays.sort;

public class T4 extends Thread {
    HashMap<Integer, String> results;
    int[] arr = new int[3];
    int index = 0;

    public T4(HashMap<Integer, String> results){
        this.results = results;
    }

    public void run(){
        System.out.println("T4 started!");

        for(int x : results.keySet()){
            arr[index] = x;
            index++;
        }

        sort(arr);

        System.out.print("T4: ");
        for(int i = 2; i >= 0; i--){
            System.out.print(results.get(arr[i]).toString() + " ");
        }
        System.out.println("\nT4 finished!\n");
    }
}
